import 'dotenv/config';
import {FB, FacebookApiException} from 'fb';
import http from 'http';
//const params = process.argv[2];
const regex = /^3[a-zA-Z0-9]{34}$/gm;

const video = process.env.FBObject;
if(typeof(video) === 'undefined') {
    throw new Error('No FB object provided');
}

const fetchAddresses = (fbobject, callback) => {
    FB.setAccessToken(process.env.FBAccessToken);
    FB.api(`/${fbobject}/comments?limit=1000`, function (res) {
        if(!res || res.error) {
         console.log(!res ? 'error occurred' : res.error);
         return false;
        }
        let results = [];
    
        for(let i = 0; i < res.data.length; i++) {
            let par = res.data[i].message;
            let found = par.match(regex);
            //console.log(res.data[i].message);
            if(found !== null) results.push(found[0]);
        }
        callback(results);
      });
      return true;
};

const prepTransfers = (amount, addresses) => {
    let transfers = [];
    for(let i = 0; i < addresses.length; i++) {
        transfers.push({
            "recipient": addresses[i],
            "amount": parseInt(amount)
        })
    }
    return transfers;
}

const massTransfer = (transfers, assetId) => {
    
    let post_options = {
        host: '127.0.0.1',
        port: 6869,
        path: '/assets/masstransfer',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'api_key': process.env.WAVES_API_KEY
        }
    };
    let fee = (transfers.length * 50000) + 100000;

    let post_data = JSON.stringify({
        'assetId': assetId,
        'sender': process.env.sender,
        'attachment' : 'thanks',
        'fee': fee,
        'transfers': transfers,
        'version': 1
    });

    let post_req = http.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log('Response: ' + chunk);
        });
    });

    post_req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
    });
    
    post_req.write(post_data);
    post_req.end();
}

fetchAddresses(video, (addresses) => {

    let transfers = prepTransfers(process.env.amount, addresses);
    let token = process.env.tokenName;
    massTransfer(transfers, process.env[token]);
});